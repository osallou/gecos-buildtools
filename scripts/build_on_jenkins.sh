#! /bin/bash

SCRIPTS_DIR=`dirname $0`
[ -z ${SCRIPTS_DIR} ] && SCRIPTS_DIR="."

JAR="${SCRIPTS_DIR}/../jenkins-api/gecos.ci.jenkins.api/dist/gecos.ci.jenkins.api-jar-with-dependencies.jar"

URL="https://ci.inria.fr/gecos"
USER=
PSSWD=
MB_JOB_NAME=
GIT_COMMIT_SHA=
BRANCH_NAME=
DOWNLOAD_DIR="artifacts"
FAIL_MODE=

parseArgs() {
    if [ $# -lt 4 ]; then
		java -jar "${JAR}"  # print usage
        exit 1
    fi

	USER="$1"
	PSSWD="$2"
	MB_JOB_NAME="$3"
	GIT_COMMIT_SHA="$4"
	[ -n "$5" ] && FAIL_MODE="$5"
	[ -n "$6" ] && BRANCH_NAME="$6"
	[ -n "$7" ] && DOWNLOAD_DIR="$7"
}

main() {
	parseArgs "$@"

	java -jar "${JAR}" "${URL}" "${USER}" "${PSSWD}" "${MB_JOB_NAME}" "${GIT_COMMIT_SHA}" \
			  "${FAIL_MODE}" "${BRANCH_NAME}" "${DOWNLOAD_DIR}"
}

main "$@"
