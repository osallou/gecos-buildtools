#! /bin/bash


#####################
## PARAMETERS
COMPOS_INDEX_FILE=p2.index
COMPOS_CONT_FILE=compositeContent.xml
COMPOS_ARTI_FILE=compositeArtifacts.xml
DRY_RUN=0
NB_PARENTS=0
DIRECTORY=
SSH_HOST=


processParams() {
    while getopts "hnr:i:c:a:" opt "$@"; do
        case "$opt" in
        h)
            print_help
            exit 0
            ;;
        n) DRY_RUN=1;;
        r) NB_PARENTS="$OPTARG";;
        i) COMPOS_INDEX_FILE="$OPTARG";;
        c) COMPOS_CONT_FILE="$OPTARG";;
        a) COMPOS_ARTI_FILE="$OPTARG";;
        esac
    done

	[[ ! "${NB_PARENTS}" =~ ^[0-9][0-9]*$ ]] && echo "[ERROR] Invalid argument NB_PARENTS ($NB_PARENTS). Must be a number" && exit 1

    shift $((OPTIND-1))
    [ "$1" = "--" ] && shift

	DIRECTORY="$1"
	SSH_HOST="$2"

	[ -z "${DIRECTORY}" ] && echo "[ERROR] No DIRECTORY is specified!" && exit 1
	[[ ! "${DIRECTORY}" =~ ^/.*$ ]] && echo "[ERROR] DIRECTORY ($DIRECTORY) must be a full path!" && exit 1
}

# = = = = = = = = = = = = = = = = = = = = 
print_help() {
    echo "Usage: $0 [OPTIONS] DIRECTORY [SSH_HOSTNAME]"
	echo ""
	echo "   DIRECTORY   : where the composite update site should be created."
	echo "                 MUST be a full path!"
	echo "   SSH_HOSTNAME: if specified, 'DIRECTORY' is considered on the remote location"
	echo "                 i.e. 'SSH_HOSTNAME:DIRECTORY'. Otherwise, it is considered locally."
	echo "   OPTIONS"
	echo "      -h                                  Display this help and exit."
	echo "      -r <nb of parents>                  Recursively apply to DIRECTORY then its parent"
	echo "                                          and so on up to <nb of parents>."
	echo "                                          Default: 0 i.e. only apply to DIRECTORY."
	echo "      -n                                  Dry run i.e. only create files locally"
	echo "                                          (do not copy them to DIRECTORY)."
	echo "      -i <p2 index filename>              the filename to be used for p2 index."
	echo "                                          Default: 'p2.index'"
	echo "      -c <composite content filename>     the filename to be used for the composite"
	echo "                                          content filename. Default: 'compositeContent.xml'"
	echo "      -a <composite artifacts filename>   the filename to be used for the composite"
	echo "                                          artifacts filename. Default: 'compositeArtifacts.xml'."
}

# = = = = = = = = = = = = = = = = = = = =
# Create/update a composite update site files for all direcoties 
# starting from $DIRECTORY up to its $NB_PARENTS parent.
main() {
	processParams "$@"

	local nb=0
    local compDir="${DIRECTORY}"
	local parent=""
    while [[ $nb -le $NB_PARENTS ]]; do
		echo "[INFO] Creating composite updatesite for ${SSH_HOST}:${compDir} ..."
        create_comp_upsite "${compDir}"
		((nb++))
#        [[ $nb > $NB_PARENTS ]] && break
		parent=`dirname "${compDir}"`
		if [ "$?" != 0 ] || [ -z "$parent" ] || [ "$parent" == "$compDir" ]; then
			echo "[WARNING] Stopping before applying to all parents. Invalid parent '$parent'"
			break
		fi
        compDir="$parent"
    done
}


# = = = = = = = = = = = = = = = = = = = =
# Create/update a composite update site files in the specified directory
# referencing all its subdirectories that contains update sites.
# Ignore if it does not contain any update site.
# 
# -$1: directory; where the composite update site should be created.
create_comp_upsite() {
	local upsiteDir="$1"

	# check directory
	if $(execC "${SSH_HOST}" "[ ! -d "${upsiteDir}" ]"); then
		echo "[ERROR] The specified directory is not valid! ${SSH_HOST}:${upsiteDir}"
		exit 1
	fi

	local timestamp=`date +%Y%m%d%H%M%S`

	# create composite updatesite files
	local children=$(get_compositeChildren "${upsiteDir}")
	if [ -z "$children" ]; then
		echo "[WARNING] Skipping directory; it does not contain any update site!"
        exit 0
    fi

	write_p2index "${COMPOS_INDEX_FILE}"
	write_compositeContent "${timestamp}" "${children}" "${COMPOS_CONT_FILE}"
	write_compositeArtifacts "${timestamp}" "${children}" "${COMPOS_ARTI_FILE}"

	# check files
	if [ $(isEmpty "${COMPOS_INDEX_FILE}") ] || [ $(isEmpty "${COMPOS_CONT_FILE}") ] || [ $(isEmpty "${COMPOS_ARTI_FILE}") ]; then
		echo "[ERROR] Failed to create composite updatesite files!"
        exit 2
	fi

	# copy them to remote location..
	if [ $DRY_RUN == 0 ]; then
		if [ ! -z "${SSH_HOST}" ]; then
			scp "${COMPOS_INDEX_FILE}" "${COMPOS_CONT_FILE}" "${COMPOS_ARTI_FILE}"  "${SSH_HOST}:${upsiteDir}"
		else
			cp "${COMPOS_INDEX_FILE}" "${COMPOS_CONT_FILE}" "${COMPOS_ARTI_FILE}"  "${upsiteDir}"
		fi
		rm "${COMPOS_INDEX_FILE}" "${COMPOS_CONT_FILE}" "${COMPOS_ARTI_FILE}"
	fi
}

# -$1: ssh hostname; if specified then $1 is executed on the remote server.
# -$2..: the commande to be executed
execC() {
	if [ -z "$1" ]; then
		eval $2
	else
		ssh "$1" $2
	fi
}

# -$1: composite dir
get_compositeChildren() {
	local children=$(execC "${SSH_HOST}" "find '$1' -maxdepth 2 -mindepth 2 -name 'p2.index' -exec dirname {} \; | uniq")
	[ -z "$children" ] && return 1

	local array=( $children ) 
	local size=${#array[@]}
	[[ $size == 0 ]] && return 1

	echo "  <children size='$size'>"
	for child in ${children}; do
		echo "    <child location='./`basename $child`'/>"
	done
	echo "  </children>"
}

# -$1: local file
isEmpty() {
	[ ! -f "$1" ] || [[ `stat -c %s "$1"` < 10 ]]
}

# -$1: toFile
write_p2index() {
    cat <<EOF > $1
version=1
metadata.repository.factory.order=${COMPOS_CONT_FILE},\!
artifact.repository.factory.order=${COMPOS_ARTI_FILE},\!
EOF
}

# -$1: timestamp
# -$2: children
# -$3: toFile
write_compositeContent() {
	cat <<EOF > $3
<?xml version='1.0' encoding='UTF-8'?>
<?compositeMetadataRepository version='1.0.0'?>
<repository name='GeCoS composite' type='org.eclipse.equinox.internal.p2.metadata.repository.CompositeMetadataRepository' version='1'>

  <properties size='1'>
    <property name='p2.timestamp' value='$1'/>
  </properties>

$2

</repository>
EOF
}

# -$1: timestamp
# -$2: children
# -$3: toFile
write_compositeArtifacts() {
	cat <<EOF > $3
<?xml version='1.0' encoding='UTF-8'?>
<?compositeArtifactRepository version='1.0.0'?>
<repository name='GeCoS composite' type='org.eclipse.equinox.internal.p2.artifact.repository.CompositeArtifactRepository' version='1'>

  <properties size='1'>
    <property name='p2.timestamp' value='$1'/>
  </properties>

$2

</repository>
EOF
}


# = = = = = = = = = = = = = = = = = = = =

main "$@"

