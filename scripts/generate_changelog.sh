#!/bin/bash


# -$1: from commit
# -$2: to commit
# -$3: entry format
# -$4: new version
git_changelog() {
	local date=`date +'%d %b %Y'`
	echo "## Release version $4 ($date)"
	echo ""
	git log $1..$2 --pretty=format:"$3"
	echo ""
	echo "---"
}

getLatestReleaseVersion() {
	git tag -l v[0-9]*.[0-9]*.[0-9]* | sort -r | head -1
}

# -$1: git root dir
# -$2: new version
main() {
	ROOT_DIR="$1"
	NEW_VER="$2"

	[ -d "$ROOT_DIR" ] || (
		echo "[ERRO] the specified directory ($ROOT_DIR) is not found!"
		exit 1
	)

	cd "$ROOT_DIR"

	local from=$(getLatestReleaseVersion)
	local to=HEAD
	local format='* %s'

	git_changelog "$from" "$to" "$format" "$NEW_VER"
}


main "$@"
