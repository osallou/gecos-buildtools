package gecos.ci.jenkins;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.offbytwo.jenkins.model.BuildWithDetails;

/**
 * 
 * @author aelmouss
 */
public class JenkinsApi {
	
	private static String url;
	private static String user;
	private static String passwd;
	private static List<FailMode> failMode;
	private static String mbName;
	private static String commitSha;
	
	private static String branchName;
	private static String artifactDownloadPath = "artifacts";

	//TODO add arg option to enable/disable printBuildingConsole
	private static boolean printBuildingConsole = false; // if true print Jenkins build console while waiting for build to finish.
	private static final int RETRY_COUNT = 5; // number of times this will try to find the jenkins build for the specified commit.

	enum FailMode {
		FAIL,				// fail if any test has failed
		SKIP,				// fail if any test was skipped
		NO_TEST,			// fail if no test was run
		REGRESSION;			//TODO fail if a regression is detected
	}
	
	private static void parseArgs(String[] args) throws UnsupportedEncodingException {
		if(args.length < 5) {
			printUsage();
			System.exit(1);
		}
		url = args[0];
		user = args[1];
		passwd = args[2];
		mbName = URLEncoder.encode(args[3], "UTF-8");
		commitSha = args[4];
		
		if(args.length > 5)parseFailMode(args[5]);
		if(args.length > 6) branchName = URLEncoder.encode(args[6], "UTF-8");
		if(args.length > 7) artifactDownloadPath = args[7];
	}

	private static void parseFailMode(String commaSeparated) {
		failMode = new ArrayList<>();
		if(commaSeparated == null || commaSeparated.isEmpty())
			return;
		String[] modes = commaSeparated.split(",");
		Arrays.stream(modes)
			.map(FailMode::valueOf)
			.forEach(failMode::add);
			
	}

	private static void printUsage() {
		StringBuilder sb = new StringBuilder();
		sb.append("Usage: program URL USER PASSWD MB_NAME COMMIT_SHA [BRANCH_NAME [ARTIFACT_DOWNLOAD_LOC]]");
		sb.append("\n   URL                  : jenkins server url");
		sb.append("\n   USER                 : jenkins user (with proper permissions)");
		sb.append("\n   PASSWD               : user's password or api token");
		sb.append("\n   MB_NAME              : multi-branch job name");
		sb.append("\n   COMMIT_SHA           : git commit sha (to which a job is associated).");
		sb.append("\n   FAIL_MODE            : [optional] specify the failure mode. A comma-separated list of one or more of the following:");
		sb.append("\n                           - 'FAIL' : fail in case any test has failed");
		sb.append("\n                           - 'SKIP' : fail in case any test wad skiped");
		sb.append("\n                           - 'NO_TEST' : fail in case no test was run");
		sb.append("\n                         If none is specified (empty), this won't fail regardless of the test result.");
		sb.append("\n   BRANCH_NAME          : [optional] the git branch name corresponding to the commit sha.");
		sb.append("\n                          If not specified, recent jobs of all branches are lookedup to");
		sb.append("\n                          find the job triggered bu commit sha.");
		sb.append("\n   ARTIFACT_DOWNLOAD_LOC: [optional] locale download directory location. Defaults to './artifacts'.");
		JenkinsConnection.error(sb.toString());
	}

	
	/**
	 * Triggers a Jenkins Multibranch build (if new commits were detected) 
	 * and returns the latest (not guaranteed!) build that have the specified 
	 * COMMIT_SHA.
	 * If the specified branch name is found, only its job is lookup for the build,
	 * otherwise, all branches are considered.
	 *  
	 * @param args program parameters
	 * @throws IOException if error occurs
	 * @throws InterruptedException if error occurs
	 * @throws TestFailException if test fails (depending of {@link #failMode})
	 */
	public static void main(String[] args) throws IOException, InterruptedException, TestFailException {
    	parseArgs(args);
    	
    	JenkinsConnection connection = new JenkinsConnection();
    	connection.connect(url, user, passwd);
    	
    	
    	/* Try to find the BranchJob build of the specified commit */
    	int retryCont = RETRY_COUNT;
    	BuildWithDetails buildDetails = null;
    	while(buildDetails == null) {
	    	try {
		    	/* Try to find the specified (git) multibranch job */
		    	MultiBranchJob mbJob = connection.findMultibranchJob(mbName);
		
		    	/*
		    	 * Trigger multibranch job build and wait for it to finish:
		    	 * 
		    	 * This will scan the SCM for changes and trigger the corresponding branch jobs.
		    	 * In case a new branch has been added, a corresponding branch job is created first.
		    	 */
		//    	Build lastBuild = connection.triggerJob(mbJob.getFolderJob());
		//    	connection.waitBuild(lastBuild.details(), printBuildingConsole);
		//    	XXX Jenkins FolderJob (MultiBranch Job) does not have information about builds, thus lastBuild is always
		//    		Build.BUILD_HAS_NEVER_RUN. Therefore it is not possible to wait for the MultiBranch job build to finish.
		//    		As a workarround we wait for 15 seconds.
		//    	TODO a proper solution can be implemented using Ocean Blue rest API.
		    	connection.triggerJob(mbJob.getFolderJob());
		    	Thread.sleep(10000);
		    	
		    	/* try to find the branch build */
		    	buildDetails = connection.findBranchBuild(mbJob, commitSha, branchName);
	    	} catch (JenkinsConnectionException e) {
				retryCont--;
				if(retryCont > 0) {
					JenkinsConnection.warn("Failed to find build. Retrying in 2 sec...");
					Thread.sleep(2000);
				} else {
					throw e;
				}
			}
    	}
    	
    	/* Wait build to finish if is still running */
    	buildDetails = connection.waitBuild(buildDetails, printBuildingConsole);
    	
    	/* Check the build state: exit if failed */
    	connection.checkBuildResult(buildDetails);
    	
    	/* Check build Test results */
    	try {
			connection.checkBuildTestResults(buildDetails, failMode);
		} catch (TestFailException e) {
			JenkinsConnection.error(e.getMessage());
			throw e;
		} catch (JenkinsConnectionException e) {
			JenkinsConnection.warn("Test results not found! Probably due no tests being added, otherwise it could be a connection problem."); 
		}
    	
		/* Download all Build artifacts */
    	connection.downloadBuildArtifacts(buildDetails, artifactDownloadPath);
    }

}
