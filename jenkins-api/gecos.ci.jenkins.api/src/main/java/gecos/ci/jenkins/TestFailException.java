package gecos.ci.jenkins;

public class TestFailException extends Exception {

	private static final long serialVersionUID = -2042775169842724809L;

	public TestFailException(String string) {
		super(string);
	}
	
}
